FROM amazoncorretto:11-al2-jdk

ARG ANT_VERSION=1.10.9
ARG WILDFLY_VERSION=21.0.2.Final

ENV ANT_HOME=/opt/ant
ENV WILDFLY_HOME=/opt/wildfly
ENV PATH=$PATH:$ANT_HOME/bin:$WILDFLY_HOME/bin

# https://docs.aws.amazon.com/efs/latest/ug/wt2-apache-web-server.html
# https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/SSL-on-amazon-linux-2.html

RUN yum update -y \
  && yum install -y tar gzip git \
  && curl -O https://downloads.apache.org/ant/binaries/apache-ant-$ANT_VERSION-bin.tar.gz \
  && tar -xzf apache-ant-$ANT_VERSION-bin.tar.gz \
  && mv apache-ant-$ANT_VERSION $ANT_HOME \
  && rm apache-ant-$ANT_VERSION-bin.tar.gz \
  && curl -O https://download.jboss.org/wildfly/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz \
  && tar -xzf wildfly-$WILDFLY_VERSION.tar.gz \
  && mv wildfly-$WILDFLY_VERSION $WILDFLY_HOME \
  && rm wildfly-$WILDFLY_VERSION.tar.gz \
  && curl -O https://repo.mysql.com/mysql80-community-release-el7-3.noarch.rpm \
  && yum install -y mysql80-community-release-el7-3.noarch.rpm \
  && rm mysql80-community-release-el7-3.noarch.rpm \
  && yum install -y mysql-community-server \
  && /usr/sbin/mysqld --initialize --user=mysql \
  && MYSQL_ROOT_PASSWORD=`grep 'temporary password' /var/log/mysqld.log  | sed 's/.*: \\(.*\\)/\\1/g'` \
  && echo $MYSQL_ROOT_PASSWORD > mysql-root-password.txt \
  && yum install -y httpd mod_ssl
